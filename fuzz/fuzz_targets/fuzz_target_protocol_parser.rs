#![no_main]
use libfuzzer_sys::fuzz_target;
use rust_vbus::protocol::Vbus;
use rust_vbus::protocol::parser::vbus_parser;

// fuzz the parser
fuzz_target!(|data: &[u8]| {
    let result = vbus_parser(data);
});