use serde::Serialize;
use strum::EnumProperty;
use thiserror::Error;

use crate::protocol::{Vbus, VbusPayload};

#[derive(Debug, Error)]
pub enum CodecError {
    #[error("invalid value id {0}")]
    InvalidValueId(String),
    #[error("invalid payload data")]
    InvalidPayload,
    #[error("invalid payload length (got {got} expected {expected})")]
    InvalidPayloadLength { got: usize, expected: usize },
}

#[derive(Debug, Copy, Clone, Serialize, PartialEq, Eq, Hash, EnumProperty)]
pub enum Field {
    #[strum(props(path = "temperature/1"))]
    TemperatureSensor1,
    #[strum(props(path = "temperature/2"))]
    TemperatureSensor2,
    #[strum(props(path = "relais/1/rpm"))]
    RpmRelais1,
    #[strum(props(path = "relais/1/operating_hours"))]
    OperatinghoursRelais1,
    #[strum(props(path = "system/time"))]
    Systemtime,
    #[strum(props(path = "system/version"))]
    SwVersion,
}

#[derive(Debug, Clone, Serialize, PartialEq)]
pub enum VbusDecoded {
    VBusV1(Vec<VbusData>),
    VbusUnsupported,
}

impl VbusDecoded {
    pub fn decode_payload(vbus: &Vbus) -> Option<Self> {
        match &vbus.get_data() {
            VbusPayload::V1(frames) if vbus.get_destination() == 0x0010 => {
                let data = frames
                    .iter()
                    .map(|frame| frame.frame_data.to_vec())
                    .collect::<Vec<Vec<u8>>>()
                    .concat();
                Some(
                    VbusDecoded::VBusV1(vec![
                        VbusData {
                            field: Field::TemperatureSensor1,
                            data: VbusValue::Temperature(
                                VbusTemperature::decode_value(&data[0..2]).ok()?,
                            ),
                        },
                        VbusData {
                            field: Field::TemperatureSensor2,
                            data: VbusValue::Temperature(
                                VbusTemperature::decode_value(&data[2..4]).ok()?,
                            ),
                        },
                        VbusData {
                            field: Field::RpmRelais1,
                            data: VbusValue::Speed(VbusPercentage::decode_value(&data[8..9]).ok()?),
                        },
                        VbusData {
                            field: Field::OperatinghoursRelais1,
                            data: VbusValue::Duration(
                                VbusDuration::decode_value(&data[9..11]).ok()?,
                            ),
                        },
                        VbusData {
                            field: Field::Systemtime,
                            data: VbusValue::Time(VbusTime::decode_value(&data[22..24]).ok()?),
                        },
                        VbusData {
                            field: Field::SwVersion,
                            data: VbusValue::Version(
                                VbusVersion::decode_value(&data[32..34]).ok()?,
                            ),
                        },
                    ])
                    .into(),
                )
            }
            VbusPayload::V1(_)
            | VbusPayload::V2 {
                datapoint_id: _,
                value: _,
            } => Some(VbusDecoded::VbusUnsupported.into()),
        }
    }
}

#[derive(Debug, Clone, Serialize, PartialEq)]
pub struct VbusTemperature(pub f32);
#[derive(Debug, Clone, Serialize, PartialEq)]
pub struct VbusPercentage(pub u8);
#[derive(Debug, Clone, Serialize, PartialEq)]
pub struct VbusDuration(pub u16);
#[derive(Debug, Clone, Serialize, PartialEq)]
pub struct VbusTime(pub u16);
#[derive(Debug, Clone, Serialize, PartialEq)]
pub struct VbusVersion(pub f32);

trait VbusValueType {
    fn encode_value(&self) -> Vec<u8>;
    fn decode_value(data: &[u8]) -> Result<Self, CodecError>
    where
        Self: Sized;
}

fn expect_payload_len(data: &[u8], expected: usize) -> Result<(), CodecError> {
    if data.len() != expected {
        Err(CodecError::InvalidPayloadLength {
            got: data.len(),
            expected,
        })?
    };
    Ok(())
}

impl VbusValueType for VbusTemperature {
    fn encode_value(&self) -> Vec<u8> {
        ((self.0 * 10.0) as i16).to_le_bytes().to_vec()
    }

    fn decode_value(data: &[u8]) -> Result<Self, CodecError>
    where
        Self: Sized,
    {
        expect_payload_len(data, 2)?;
        Ok(VbusTemperature(
            f32::from(i16::from_le_bytes(data[0..2].try_into().unwrap())) / 10.0,
        ))
    }
}

impl VbusValueType for VbusPercentage {
    fn encode_value(&self) -> Vec<u8> {
        vec![self.0]
    }

    fn decode_value(data: &[u8]) -> Result<Self, CodecError>
    where
        Self: Sized,
    {
        expect_payload_len(data, 1)?;
        Ok(VbusPercentage(data[0]))
    }
}

impl VbusValueType for VbusDuration {
    fn encode_value(&self) -> Vec<u8> {
        (self.0 as u16).to_le_bytes().to_vec()
    }

    fn decode_value(data: &[u8]) -> Result<Self, CodecError>
    where
        Self: Sized,
    {
        expect_payload_len(data, 2)?;
        Ok(VbusDuration(u16::from_le_bytes(
            data[0..2].try_into().unwrap(),
        )))
    }
}

impl VbusValueType for VbusTime {
    fn encode_value(&self) -> Vec<u8> {
        (self.0 as u16).to_le_bytes().to_vec()
    }

    fn decode_value(data: &[u8]) -> Result<Self, CodecError>
    where
        Self: Sized,
    {
        expect_payload_len(data, 2)?;
        Ok(VbusTime(u16::from_le_bytes(data[0..2].try_into().unwrap())))
    }
}

impl VbusValueType for VbusVersion {
    fn encode_value(&self) -> Vec<u8> {
        ((self.0 * 100 as f32) as u16).to_le_bytes().to_vec()
    }

    fn decode_value(data: &[u8]) -> Result<Self, CodecError>
    where
        Self: Sized,
    {
        expect_payload_len(data, 2)?;
        Ok(VbusVersion(
            f32::from(u16::from_le_bytes(data[0..2].try_into().unwrap())) / 100.0,
        ))
    }
}

#[derive(Debug, Clone, Serialize, PartialEq)]
pub enum VbusValue {
    Temperature(VbusTemperature),
    Speed(VbusPercentage),
    Duration(VbusDuration),
    Time(VbusTime),
    Version(VbusVersion),
}

#[derive(Debug, Clone, Serialize, PartialEq)]
pub struct VbusData {
    field: Field,
    data: VbusValue,
}

impl TryFrom<&Vbus> for Vec<VbusData> {
    type Error = CodecError;

    fn try_from(vbus: &Vbus) -> Result<Self, Self::Error> {
        let Some(VbusDecoded::VBusV1(decoded)) = VbusDecoded::decode_payload(vbus) else {
            Err(CodecError::InvalidPayload)?
        };
        Ok(decoded)
    }
}

impl VbusData {
    pub fn get_field(&self) -> &Field {
        &self.field
    }

    pub fn get_data(&self) -> &VbusValue {
        &self.data
    }

    /// a string representation of the field path
    pub fn get_field_path_str(&self) -> &'static str {
        self.field.get_str("path").unwrap()
    }

    /// a raw string representation without units
    pub fn get_value_raw_string(&self) -> String {
        match &self.data {
            VbusValue::Temperature(vbus_temperature) => format!("{}", vbus_temperature.0),
            VbusValue::Speed(vbus_percentage) => format!("{}", vbus_percentage.0),
            VbusValue::Duration(vbus_duration) => format!("{}", vbus_duration.0),
            VbusValue::Time(vbus_time) => format!("{}", vbus_time.0),
            VbusValue::Version(vbus_version) => format!("{}", vbus_version.0),
        }
    }
}

fn calc_value_id_hash(value_id: &str) -> Result<u32, CodecError> {
    let mut hash: u32 = 0;
    if !value_id.is_empty() && value_id.is_ascii() {
        for char in value_id.chars() {
            let charcode = char as u32;
            hash = (hash.overflowing_mul(0x21).0.overflowing_add(charcode).0) & 0x7FFF_FFFF;
        }
    } else {
        return Err(CodecError::InvalidValueId(value_id.to_string()));
    }
    Ok(hash)
}

#[cfg(test)]
mod tests {
    use crate::{
        codec::{
            calc_value_id_hash, Field, VbusData, VbusDuration, VbusPayload, VbusPercentage,
            VbusTemperature, VbusTime, VbusValue, VbusValueType, VbusVersion,
        },
        protocol::{Vbus, VbusFrame},
    };

    use super::VbusDecoded;

    #[test]
    fn test_decode() {
        let testcase = Vbus::new(
            16,
            4386,
            256,
            16,
            VbusPayload::V1(vec![
                VbusFrame {
                    frame_data: [97, 1, 150, 1],
                },
                VbusFrame {
                    frame_data: [184, 34, 184, 34],
                },
                VbusFrame {
                    frame_data: [0, 255, 7, 20],
                },
                VbusFrame {
                    frame_data: [0, 255, 0, 0],
                },
                VbusFrame {
                    frame_data: [11, 1, 255, 255],
                },
                VbusFrame {
                    frame_data: [0, 0, 78, 3],
                },
                VbusFrame {
                    frame_data: [0, 0, 0, 0],
                },
                VbusFrame {
                    frame_data: [0, 0, 0, 0],
                },
                VbusFrame {
                    frame_data: [100, 0, 0, 0],
                },
                VbusFrame {
                    frame_data: [15, 39, 25, 252],
                },
            ]),
        );
        let want = VbusDecoded::VBusV1(vec![
            VbusData {
                field: Field::TemperatureSensor1,
                data: VbusValue::Temperature(VbusTemperature(35.3)),
            },
            VbusData {
                field: Field::TemperatureSensor2,
                data: VbusValue::Temperature(VbusTemperature(40.6)),
            },
            VbusData {
                field: Field::RpmRelais1,
                data: VbusValue::Speed(VbusPercentage(0)),
            },
            VbusData {
                field: Field::OperatinghoursRelais1,
                data: VbusValue::Duration(VbusDuration(2047)),
            },
            VbusData {
                field: Field::Systemtime,
                data: VbusValue::Time(VbusTime(846)),
            },
            VbusData {
                field: Field::SwVersion,
                data: VbusValue::Version(VbusVersion(1.0)),
            },
        ]);
        assert_eq!(want, VbusDecoded::decode_payload(&testcase).unwrap());
    }

    #[test]
    fn test_decode_temperature() {
        let testcase = vec![97, 1];
        let want = VbusTemperature(35.3);
        assert_eq!(want, VbusTemperature::decode_value(&testcase).unwrap());
    }

    #[test]
    fn test_encode_temperature() {
        let testcase = VbusTemperature(35.3);
        let want = vec![97, 1];
        assert_eq!(want, VbusTemperature::encode_value(&testcase));
    }

    #[test]
    fn test_decode_percentage() {
        let testcase = vec![10];
        let want = VbusPercentage(10);
        assert_eq!(want, VbusPercentage::decode_value(&testcase).unwrap());
    }

    #[test]
    fn test_encode_percentage() {
        let testcase = VbusPercentage(10);
        let want = vec![10];
        assert_eq!(want, VbusPercentage::encode_value(&testcase));
    }

    #[test]
    fn test_decode_duration() {
        let testcase = vec![255, 7];
        let want = VbusDuration(2047);
        assert_eq!(want, VbusDuration::decode_value(&testcase).unwrap());
    }

    #[test]
    fn test_encode_duration() {
        let testcase = VbusDuration(2047);
        let want = vec![255, 7];
        assert_eq!(want, VbusDuration::encode_value(&testcase));
    }

    #[test]
    fn test_decode_time() {
        let testcase = vec![78, 3];
        let want = VbusTime(846);
        assert_eq!(want, VbusTime::decode_value(&testcase).unwrap());
    }

    #[test]
    fn test_encode_time() {
        let testcase = VbusTime(846);
        let want = vec![78, 3];
        assert_eq!(want, VbusTime::encode_value(&testcase));
    }

    #[test]
    fn test_decode_version() {
        let testcase = vec![100, 0];
        let want = VbusVersion(1.0);
        assert_eq!(want, VbusVersion::decode_value(&testcase).unwrap());
    }

    #[test]
    fn test_encode_version() {
        let testcase = VbusVersion(1.0);
        let want = vec![100, 0];
        assert_eq!(want, VbusVersion::encode_value(&testcase));
    }

    #[test]
    fn test_value_id_hash1() {
        let testcase_id = "Handbetrieb1";
        let want_id_hash = 1887930473;
        assert_eq!(want_id_hash, calc_value_id_hash(testcase_id).unwrap())
    }

    #[test]
    fn test_value_id_hash2() {
        let testcase_id = "Handbetrieb2";
        let want_id_hash = 1887930474;
        assert_eq!(want_id_hash, calc_value_id_hash(testcase_id).unwrap());
    }

    #[test]
    fn test_field_path() {
        let testcase = VbusData {
            field: Field::TemperatureSensor1,
            data: VbusValue::Temperature(VbusTemperature(35.3)),
        };
        let want = "temperature/1";
        assert_eq!(want, testcase.get_field_path_str())
    }

    #[test]
    fn test_value_str() {
        let testcase = VbusData {
            field: Field::TemperatureSensor1,
            data: VbusValue::Temperature(VbusTemperature(35.3)),
        };
        let want = "35.3".to_string();
        assert_eq!(want, testcase.get_value_raw_string())
    }
}
