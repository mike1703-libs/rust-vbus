use std::convert::TryInto;
use std::num::Wrapping;

use cookie_factory::bytes::{le_u16, le_u8};
use cookie_factory::combinator::slice;
use cookie_factory::gen;
use cookie_factory::multi::many_ref;
use cookie_factory::sequence::tuple;
use nom_language::error::VerboseErrorKind;
use parser::vbus_parser;
use serde::ser::SerializeStructVariant;
use serde::Serialize;
use strum::{AsRefStr, Display, EnumString, IntoStaticStr};

pub mod parser;

#[derive(Clone, Debug, Serialize, PartialEq)]
pub struct VbusFrame {
    pub frame_data: [u8; 4],
}

impl VbusFrame {
    fn _vbus_extract_septett(buffer: &[u8; 4]) -> [u8; 5] {
        let mut septett = 0;
        let mut result: [u8; 5] = [0; 5];
        for i in 0..4 {
            result[i] = buffer[i] & 0x7F;
            if (buffer[i] & 0x80) != 0 {
                septett |= 1 << i;
            }
        }
        result[4] = septett;
        result
    }

    fn _vbus_inject_septett(buffer: &[u8]) -> [u8; 4] {
        let septett = &buffer[4];
        let mut result: [u8; 4] = [0; 4];
        for i in 0..4 {
            let high_bit = if septett & (1 << i) != 0 { 0x80 } else { 0x00 };
            result[i] = buffer[i] | high_bit;
        }
        result
    }

    /// generate from original data
    fn new_from_data(data: &[u8]) -> Self {
        assert_eq!(data.len(), 4);
        // the try_into() is safe with the assert
        Self {
            frame_data: data.try_into().unwrap(),
        }
    }

    /// generate from the format that is used on the bus
    fn new(data: &[u8]) -> Self {
        assert_eq!(data.len(), 5);
        let frame_data = VbusFrame::_vbus_inject_septett(data);
        Self { frame_data }
    }

    fn appliance_serialize(&self) -> [u8; 5] {
        VbusFrame::_vbus_extract_septett(&self.frame_data)
    }
}

impl From<&u32> for VbusFrame {
    fn from(value: &u32) -> Self {
        Self {
            frame_data: value.to_le_bytes(),
        }
    }
}

impl From<VbusFrame> for u32 {
    fn from(frame: VbusFrame) -> Self {
        u32::from_le_bytes(frame.frame_data)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum VbusPayload {
    V1(Vec<VbusFrame>),
    V2 { datapoint_id: u16, value: u32 },
}

impl Serialize for VbusPayload {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        match self {
            VbusPayload::V1(frames) => {
                // collect all data frames into one vector
                let data = frames
                    .iter()
                    .map(|frame| frame.frame_data.to_vec())
                    .collect::<Vec<Vec<u8>>>()
                    .concat();

                // specify type: index: 0 VbusData::V1, 1 field
                let mut variant_serializer =
                    serializer.serialize_struct_variant("VbusData", 0, "V1", 1)?;
                // serialize this vector
                variant_serializer.serialize_field("value", &data)?;
                variant_serializer.end()
            }
            VbusPayload::V2 {
                datapoint_id,
                value,
            } => {
                // specify type: index: 1 VbusData::V2, 2 fields
                let mut variant_serializer =
                    serializer.serialize_struct_variant("VbusData", 1, "V2", 2)?;
                // serialize this vector
                variant_serializer.serialize_field("datapoint_id", datapoint_id)?;
                variant_serializer.serialize_field("value", value)?;
                variant_serializer.end()
            }
        }
    }
}

pub enum CommandType {
    Unknown,
    Ack,
    SetValue,
    GetValue,
    TransferMaster,
    ReturnMaster,
    GetValueIdHashById,
    GetValueIdByIdHash,
}

impl From<u16> for CommandType {
    fn from(value: u16) -> Self {
        match value {
            0x0100 => Self::Ack,
            0x0200 => Self::SetValue,
            0x0300 => Self::GetValue,
            0x0500 => Self::TransferMaster,
            0x0600 => Self::ReturnMaster,
            0x1000 => Self::GetValueIdHashById,
            0x1100 => Self::GetValueIdByIdHash,
            _ => Self::Unknown,
        }
    }
}

impl From<CommandType> for u16 {
    fn from(value: CommandType) -> Self {
        match value {
            CommandType::Unknown => 0x0000,
            CommandType::Ack => 0x0100,
            CommandType::SetValue => 0x0200,
            CommandType::GetValue => 0x0300,
            CommandType::TransferMaster => 0x0500,
            CommandType::ReturnMaster => 0x0600,
            CommandType::GetValueIdHashById => 0x1000,
            CommandType::GetValueIdByIdHash => 0x1100,
        }
    }
}

#[derive(Clone, Debug, Serialize, PartialEq)]
pub struct Vbus {
    destination_address: u16,
    source_address: u16,
    command: u16,
    protocol_version: u8,
    data: VbusPayload,
}

pub const SYNCBYTE: u8 = 0xaa;

impl Vbus {
    #[allow(dead_code)]
    pub fn new(
        destination_address: u16,
        source_address: u16,
        command: u16,
        protocol_version: u8,
        data: VbusPayload,
    ) -> Self {
        Vbus {
            destination_address,
            source_address,
            command,
            protocol_version,
            data,
        }
    }
}

impl VbusPayload {
    /// serialize the payload without valid checksums. These neeed to be calculated at the end
    fn serialize<'a, W: std::io::Write + 'a>(&'a self) -> impl cookie_factory::SerializeFn<W> + 'a {
        move |out: cookie_factory::WriteContext<W>| match self {
            VbusPayload::V1(ref frames) => {
                tuple((
                    le_u8(frames.len() as u8),
                    // empty checksum => generate checksum once everything is written
                    le_u8(0),
                    many_ref(frames, |frame| {
                        let frame_bytes = frame.appliance_serialize();
                        tuple((slice(frame_bytes), le_u8(generate_checksum(&frame_bytes))))
                    }),
                ))(out)
            }
            VbusPayload::V2 {
                datapoint_id,
                value,
            } => tuple((
                le_u16(*datapoint_id),
                slice(VbusFrame::from(value).appliance_serialize()),
                // empty checksum => generate checksum once everything is written
                le_u8(0),
            ))(out),
        }
    }
}

#[derive(Display, AsRefStr, IntoStaticStr, EnumString, Debug, PartialEq)]
pub enum VbusParseErrorKind {
    ChecksumError,
    DataFrameError,
    Unsupported,
}

pub enum VbusParseResult<'a> {
    /// Successfully parsed a vbus message and unparsed rest
    Ok { rest: &'a [u8], message: Vbus },
    /// There was not enough data
    Incomplete,
    /// Unrecoverable Error, broken data and unparsed rest
    Failure {
        rest: &'a [u8],
        broken_data: &'a [u8],
        error: VbusParseErrorKind,
    },
}

impl Vbus {
    pub fn serialize(&self) -> Vec<u8> {
        let calculated_len = match &self.data {
            VbusPayload::V1(frames) => 1 + 2 + 2 + 1 + 2 + 1 + 1 + frames.len() * 6,
            VbusPayload::V2 { .. } => 1 + 2 + 2 + 1 + 2 + 2 + 4 + 1 + 1,
        };
        let mut buffer = vec![0; calculated_len];
        // create the packet (with header checksum 0)
        let (buffer_ref, pos) = gen(
            tuple((
                le_u8(SYNCBYTE),
                le_u16(self.destination_address),
                le_u16(self.source_address),
                le_u8(self.protocol_version),
                le_u16(self.command),
                self.data.serialize(),
            )),
            buffer.as_mut_slice(),
        )
        .unwrap();
        assert!(buffer_ref.is_empty());
        // do header checksums now that everything else is written
        match self.data {
            VbusPayload::V1(_) => {
                gen(le_u8(generate_checksum(&buffer[1..9])), &mut buffer[9..]).unwrap()
            }
            VbusPayload::V2 { .. } => {
                gen(le_u8(generate_checksum(&buffer[1..15])), &mut buffer[15..]).unwrap()
            }
        };
        assert!(pos as usize == calculated_len);
        buffer
    }

    /// parse the input stream with the vbus parser and returns an own vbus parse result together with the rest of the input
    pub fn parse(data: &[u8]) -> VbusParseResult {
        match vbus_parser(data) {
            Ok((rest, message)) => VbusParseResult::Ok { rest, message },
            Err(error) => match error {
                nom::Err::Incomplete(_n) => VbusParseResult::Incomplete,
                // treat recoverable errors and failures the same
                nom::Err::Error(error) | nom::Err::Failure(error) => {
                    // the last error contains the real error
                    let (rest, verbose_error) = error.errors.last().unwrap();
                    let error = match verbose_error {
                        // unfortunately errors can only be reported with context strings... but this code is backed with enums
                        VerboseErrorKind::Context(context) => {
                            VbusParseErrorKind::try_from(*context).unwrap()
                        }
                        // the next two parsers cannot happen due to parser construction
                        VerboseErrorKind::Char(_) | VerboseErrorKind::Nom(_) => {
                            panic!("last error should be a Context error: {:?}", error.errors);
                        }
                    };
                    VbusParseResult::Failure {
                        rest,
                        broken_data: data,
                        error,
                    }
                }
            },
        }
    }

    // TODO: not really necessary anymore
    pub fn is_frame_start(&byte: &u8) -> bool {
        byte == SYNCBYTE
    }

    pub fn preprocess(_bytes: &[u8]) -> Vec<u8> {
        _bytes.into()
    }

    pub fn get_data(&self) -> VbusPayload {
        self.data.clone()
    }

    pub fn get_source(&self) -> u16 {
        self.source_address
    }

    pub fn get_destination(&self) -> u16 {
        self.destination_address
    }

    pub fn get_command(&self) -> u16 {
        self.command
    }

    pub fn get_version(&self) -> u8 {
        self.protocol_version
    }
}

// code was taken from specification from https://github.com/danielwippermann/resol-vbus
fn generate_checksum(buffer: &[u8]) -> u8 {
    const CRC_MAGIC: u8 = 0x7f;
    let mut crc = CRC_MAGIC;
    for buffer_byte in buffer {
        crc = (Wrapping(crc) - Wrapping(*buffer_byte)).0 & CRC_MAGIC;
    }
    crc
}

#[cfg(test)]
mod tests {
    use crate::protocol::{
        CommandType, Vbus, VbusFrame, VbusParseErrorKind, VbusParseResult, VbusPayload,
    };

    #[test]
    fn test_serialize_v1_message() {
        let vbus = Vbus::new(
            0x1234,
            0x5678,
            CommandType::Ack.into(),
            0x10,
            VbusPayload::V1(vec![VbusFrame::new_from_data(&[0x23, 0x24, 0xFE, 0xFF])]),
        );
        let want = [
            0xAA, 0x34, 0x12, 0x78, 0x56, 0x10, 0, 1, 1, 89, 0x23, 0x24, 0x7E, 0x7F, 0b1100, 47,
        ];
        let serialized = vbus.serialize();
        assert_eq!(serialized, want);
    }

    #[test]
    fn test_serialize_v2_message() {
        let vbus = Vbus::new(
            0x1234,
            0x5678,
            CommandType::Ack.into(),
            0x20,
            VbusPayload::V2 {
                datapoint_id: 0xBEEF,
                value: 0x2324FEFF,
            },
        );
        let want = [
            0xAA, 0x34, 0x12, 0x78, 0x56, 0x20, 0, 1, 0xEF, 0xBE, 0x7F, 0x7E, 0x24, 0x23, 0b0011,
            86,
        ];
        let serialized = vbus.serialize();
        assert_eq!(serialized, want);
    }

    #[test]
    fn test_vbus_parse_two_messages_with_garbage_between() {
        //
        let data = [
            0xAA, 0x34, 0x12, 0x78, 0x56, 0x20, 0, 1, 0xEF, 0xBE, 0x7F, 0x7E, 0x24, 0x23, 0b0011,
            86, // first message
            0, 0, // garbage
            0xAA, 0x45, 0x23, 0x78, 0x56, 0x20, 0, 1, 0xEF, 0xBE, 0x7F, 0x7E, 0x24, 0x23, 0b0011,
            86, // second message, correct checksum would be 52
        ];

        let VbusParseResult::Ok { rest, message } = Vbus::parse(&data) else {
            panic!()
        };
        // check the first message
        assert_eq!(message.destination_address, 0x1234);
        // second message starting with the garbage is in rest
        assert!(!rest.is_empty());
        let VbusParseResult::Failure {
            rest,
            broken_data,
            error,
        } = Vbus::parse(&rest)
        else {
            panic!()
        };
        assert_eq!(broken_data, &data[16..]);
        assert!(!rest.is_empty());
        // the second message has a wrong byte in the end
        assert_eq!(error, VbusParseErrorKind::ChecksumError);
        // and the rest is incomplete
        let VbusParseResult::Incomplete = Vbus::parse(&rest) else {
            panic!()
        };
    }
}
