use nom::bytes::streaming::{tag, take, take_till};
use nom::combinator::{map, peek, verify};
use nom::error::context;
use nom::multi::count;
use nom::number::streaming::{le_u16, u8};
use nom::Parser as _;
use nom_language::error::VerboseError;

use super::{generate_checksum, Vbus, VbusFrame, VbusParseErrorKind, VbusPayload, SYNCBYTE};

pub type ParseResult<T, U> = nom::IResult<T, U, VerboseError<T>>;

/// parse a v1 frame
fn vbus_v1_frame_parser(input: &[u8]) -> ParseResult<&[u8], VbusFrame> {
    let (input, frame_bytes) = take(5usize)(input)?;
    let (input, _frame_checksum) = context(
        VbusParseErrorKind::ChecksumError.as_ref(),
        verify(u8, |&frame_checksum| {
            frame_checksum == generate_checksum(&frame_bytes)
        }),
    )
    .parse(input)?;
    let vbus_frame = VbusFrame::new(frame_bytes);
    Ok((input, vbus_frame))
}

/// match until SOF
fn take_until_sof(input: &[u8]) -> ParseResult<&[u8], &[u8]> {
    take_till(|b| b == SYNCBYTE)(input)
}

/// parses a vbus message, drops leading garbage bytes if possible
pub fn vbus_parser(input: &[u8]) -> ParseResult<&[u8], Vbus> {
    let (input, _) = take_until_sof(input)?;
    let (input, _) = tag(&[SYNCBYTE][..])(input)?;
    let (input, header_bytes_for_checksum_v1) = peek(take(8usize)).parse(input)?;
    let (input, header_bytes_for_checksum_v2) = peek(take(14usize)).parse(input)?;
    let (input, destination_address) = le_u16(input)?;
    let (input, source_address) = le_u16(input)?;
    let (input, protocol_version) = context(
        VbusParseErrorKind::Unsupported.as_ref(),
        verify(u8, |&protocol_version| {
            protocol_version == 0x10 || protocol_version == 0x20
        }),
    )
    .parse(input)?;
    let (input, command) = le_u16(input)?;
    let (input, data) = match protocol_version {
        0x10 => {
            let (input, payload_frames) = u8(input)?;
            let (input, _header_checksum) = context(
                VbusParseErrorKind::ChecksumError.as_ref(),
                verify(u8, |header_checksum| {
                    *header_checksum == generate_checksum(header_bytes_for_checksum_v1)
                }),
            )
            .parse(input)?;
            // ensure that the right amount of bytes is already available as count is failing with a Nom(Count) error otherwise
            let _ = peek(take(payload_frames as usize * 5)).parse(input)?;
            let (input, frames) = context(
                VbusParseErrorKind::DataFrameError.as_ref(),
                count(vbus_v1_frame_parser, payload_frames.into()),
            )
            .parse(input)?;
            (input, VbusPayload::V1(frames))
        }
        0x20 => {
            let (input, datapoint_id) = le_u16(input)?;
            let (input, value) = map(take(5usize), |value| VbusFrame::new(value)).parse(input)?;
            let (input, _full_checksum) = context(
                VbusParseErrorKind::ChecksumError.as_ref(),
                verify(u8, |full_checksum| {
                    *full_checksum == generate_checksum(header_bytes_for_checksum_v2)
                }),
            )
            .parse(input)?;
            (
                input,
                VbusPayload::V2 {
                    datapoint_id,
                    value: value.into(),
                },
            )
        }
        _ => {
            // verification of the protocol_version ensures this is not executed
            unimplemented!();
        }
    };

    Ok((
        input,
        Vbus::new(
            destination_address,
            source_address,
            command,
            protocol_version,
            data,
        ),
    ))
}

#[cfg(test)]
mod tests {
    use nom_language::error::VerboseErrorKind;

    use crate::protocol::{parser::vbus_parser, VbusFrame, VbusParseErrorKind, VbusPayload};

    use super::Vbus;

    #[test]
    fn test_wrong_version() {
        let data = [170, 0, 0, 0, 0, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        let nom::Err::Error(result) = vbus_parser(&data).unwrap_err() else {
            panic!()
        };
        assert_eq!(
            result.errors[1].1,
            VerboseErrorKind::Context(VbusParseErrorKind::Unsupported.as_ref())
        );
    }

    #[test]
    fn test_ok_v1_one_frame() {
        let data = [170, 0, 0, 0, 0, 0x10, 0, 0, 1, 110, 0, 0, 0, 0, 0, 127];
        let (rest, vbus) = vbus_parser(&data).unwrap();
        assert!(rest.is_empty());
        let want = Vbus {
            destination_address: 0,
            source_address: 0,
            command: 0,
            protocol_version: 16,
            data: VbusPayload::V1(
                [VbusFrame {
                    frame_data: [0, 0, 0, 0],
                }]
                .to_vec(),
            ),
        };
        assert_eq!(vbus, want);
    }

    #[test]
    fn test_ok_v1_255_frame() {
        let frame = vec![0u8, 0, 0, 0, 0, 127];
        let mut data = vec![170, 0, 0, 0, 0, 0x10, 0, 0, 255, 112];
        for _ in 0..255 {
            data.extend(frame.clone())
        }
        let (rest, vbus) = vbus_parser(&data).unwrap();
        assert!(rest.is_empty());
        let VbusPayload::V1(frames) = vbus.data else {
            panic!()
        };
        assert_eq!(frames.len(), 255);
    }

    #[test]
    fn test_invalid_header_checksum() {
        let data = [170, 0, 0, 0, 0, 0x10, 0, 0, 0, 42, 0, 0, 0, 0, 0];
        let nom::Err::Error(result) = vbus_parser(&data).unwrap_err() else {
            panic!()
        };
        assert_eq!(
            result.errors[1].1,
            VerboseErrorKind::Context(VbusParseErrorKind::ChecksumError.as_ref())
        );
        assert_eq!(result.errors[1].0, [42, 0, 0, 0, 0, 0]);
    }

    #[test]
    fn test_datablock_checksum_error() {
        // the (first) data frame should have checksum 127 but has a wrong 0x42
        // this fails the vbus_v1_frame_parser with a ChecksumError, that causes the count
        // to fail with a Nom(Count) error that is wrapper with a DataBlockError context
        let data = [
            170, 0, 0, 0, 0, 0x10, 0, 0, 1, 110, 0, 0, 0, 0, 0, 0x42, // should be 127
        ];
        let nom::Err::Error(result) = vbus_parser(&data).unwrap_err() else {
            panic!()
        };
        assert_eq!(
            result.errors.last().unwrap().1,
            VerboseErrorKind::Context(VbusParseErrorKind::DataFrameError.as_ref())
        );
    }

    #[test]
    fn test_leading_garbage_then_ok() {
        let data = [
            0, 1, 2, 3, 170, 0, 0, 0, 0, 0x10, 0, 0, 1, 110, 0, 0, 0, 0, 0, 127,
        ];
        let (rest, vbus) = vbus_parser(&data).unwrap();
        assert!(rest.is_empty());
        assert_eq!(vbus.protocol_version, 0x10);
    }

    #[test]
    fn test_only_garbage() {
        let data = [
            0, 1, 2, 3, 42, 0, 0, 0, 0, 0x10, 0, 0, 1, 110, 0, 0, 0, 0, 0, 127,
        ];
        let error = vbus_parser(&data).unwrap_err();
        assert_eq!(
            error,
            nom::Err::Incomplete(nom::Needed::Size(std::num::NonZeroUsize::new(1).unwrap()))
        );
    }

    #[test]
    fn test_ok_v2_datapoint() {
        let data = [170, 0, 0, 0, 0, 0x20, 0, 0, 1, 2, 42, 43, 44, 45, 0, 46];
        let (rest, vbus) = vbus_parser(&data).unwrap();
        assert!(rest.is_empty());
        let VbusPayload::V2 {
            datapoint_id,
            value,
        } = vbus.data
        else {
            panic!()
        };
        assert_eq!(datapoint_id, 0x0201);
        assert_eq!(value, 0x2d2c2b2a);
    }

    #[test]
    fn test_incomplete_message() {
        let data = [170, 0, 0, 0, 0, 0x10];
        let error = vbus_parser(&data).unwrap_err();
        assert_eq!(
            error,
            nom::Err::Incomplete(nom::Needed::Size(std::num::NonZeroUsize::new(3).unwrap()))
        );
    }

    #[test]
    fn test_v1_regular_message() {
        let data = [
            170, 21, 0, 34, 17, 16, 0, 1, 10, 28, 1, 8, 0, 0, 0, 118, 0, 0, 127, 127, 12, 117, 2,
            10, 0, 0, 0, 115, 22, 1, 22, 1, 5, 76, 0, 0, 0, 0, 0, 127, 1, 11, 0, 0, 0, 115, 0, 0,
            0, 0, 0, 127, 2, 1, 0, 11, 0, 113, 22, 1, 22, 1, 5, 76, 56, 34, 56, 34, 5, 70,
        ];
        let (rest, result) = vbus_parser(&data).unwrap();
        let want = Vbus::new(
            21,
            4386,
            256,
            16,
            VbusPayload::V1(vec![
                VbusFrame {
                    frame_data: [1, 8, 0, 0],
                },
                VbusFrame {
                    frame_data: [0, 0, 255, 255],
                },
                VbusFrame {
                    frame_data: [2, 10, 0, 0],
                },
                VbusFrame {
                    frame_data: [150, 1, 150, 1],
                },
                VbusFrame {
                    frame_data: [0, 0, 0, 0],
                },
                VbusFrame {
                    frame_data: [1, 11, 0, 0],
                },
                VbusFrame {
                    frame_data: [0, 0, 0, 0],
                },
                VbusFrame {
                    frame_data: [2, 1, 0, 11],
                },
                VbusFrame {
                    frame_data: [150, 1, 150, 1],
                },
                VbusFrame {
                    frame_data: [184, 34, 184, 34],
                },
            ]),
        );
        assert_eq!(want, result);
        assert!(rest.is_empty());
    }

    #[test]
    fn test_v1_message_2() {
        let data = [
            170, 16, 0, 34, 17, 16, 0, 1, 10, 33, 97, 1, 22, 1, 4, 2, 56, 34, 56, 34, 5, 70, 0,
            127, 7, 20, 2, 99, 0, 127, 0, 0, 2, 126, 11, 1, 127, 127, 12, 105, 0, 0, 78, 3, 0, 46,
            0, 0, 0, 0, 0, 127, 0, 0, 0, 0, 0, 127, 100, 0, 0, 0, 0, 27, 15, 39, 25, 124, 8, 44,
        ];
        let (rest, result) = vbus_parser(&data).unwrap();
        let want = Vbus::new(
            16,
            4386,
            256,
            16,
            VbusPayload::V1(vec![
                VbusFrame {
                    frame_data: [97, 1, 150, 1],
                },
                VbusFrame {
                    frame_data: [184, 34, 184, 34],
                },
                VbusFrame {
                    frame_data: [0, 255, 7, 20],
                },
                VbusFrame {
                    frame_data: [0, 255, 0, 0],
                },
                VbusFrame {
                    frame_data: [11, 1, 255, 255],
                },
                VbusFrame {
                    frame_data: [0, 0, 78, 3],
                },
                VbusFrame {
                    frame_data: [0, 0, 0, 0],
                },
                VbusFrame {
                    frame_data: [0, 0, 0, 0],
                },
                VbusFrame {
                    frame_data: [100, 0, 0, 0],
                },
                VbusFrame {
                    frame_data: [15, 39, 25, 252],
                },
            ]),
        );
        assert_eq!(want, result);
        assert!(rest.is_empty());
    }

    #[test]
    fn test_v2_regular_message() {
        let data = [170, 0, 0, 34, 17, 32, 0, 5, 0, 0, 0, 0, 0, 0, 0, 39];
        let (rest, vbus) = vbus_parser(&data).unwrap();
        let want = Vbus::new(
            0,
            4386,
            1280,
            32,
            VbusPayload::V2 {
                datapoint_id: 0,
                value: 0,
            },
        );
        assert_eq!(want, vbus);
        assert!(rest.is_empty());
    }

    #[test]
    fn test_two_ok_messages_v1_v2() {
        let data = [
            170, 0, 0, 0, 0, 0x10, 0, 0, 1, 110, 0, 0, 0, 0, 0, 127, // first
            170, 0, 0, 34, 17, 0x20, 0, 5, 0, 0, 0, 0, 0, 0, 0, 39, // second
        ];
        // parse first message
        let (rest, vbus) = vbus_parser(&data).unwrap();
        assert!(!rest.is_empty());
        assert_eq!(vbus.protocol_version, 0x10);
        // parse second message
        let (rest, vbus) = vbus_parser(&rest).unwrap();
        assert!(rest.is_empty());
        assert_eq!(vbus.protocol_version, 0x20);
    }
}
